import { IonicModule } from "@ionic/angular";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ListasComponent } from "./listas/listas.component";
import { PipesModule } from "../pipes/pipes.module";

@NgModule({
  declarations: [ListasComponent],
  imports: [IonicModule, CommonModule, PipesModule],
  exports: [ListasComponent]
})
export class ComponentsModule {}
