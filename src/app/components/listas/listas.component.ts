import { Component, OnInit, Input, ViewChild } from "@angular/core";
import { Lista } from "src/app/models/lista.model";
import { DeseosService } from "src/app/services/deseos.service";
import { Router } from "@angular/router";
import { AlertController, IonList } from "@ionic/angular";

@Component({
  selector: "app-listas",
  templateUrl: "./listas.component.html",
  styleUrls: ["./listas.component.scss"]
})
export class ListasComponent implements OnInit {
  @ViewChild(IonList) lista: IonList;
  @Input() terminado = true;
  constructor(
    public deseosService: DeseosService,
    private router: Router,
    private alertCtrl: AlertController
  ) {}

  ngOnInit() {}
  listaSeleccionada(lista: Lista) {
    if (this.terminado) {
      this.router.navigateByUrl(`/tabs/tab2/agregar/${lista.id}`);
    } else {
      this.router.navigateByUrl(`/tabs/tab1/agregar/${lista.id}`);
    }
  }
  borrarLista(item: Lista) {
    this.deseosService.borrarLista(item);
  }
  async editarTitle(item: Lista) {
    const alert = await this.alertCtrl.create({
      header: "Nueva lista",
      inputs: [
        {
          name: "titulo",
          type: "text",
          value: item.titulo,
          placeholder: "Nombre de la lista"
        }
      ],
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
            this.lista.closeSlidingItems();
          }
        },
        {
          text: "Cambiar",
          handler: data => {
            if (data.titulo.length === 0) {
              return;
            }
            this.deseosService.editarTitle(data.titulo, item.titulo);
            this.lista.closeSlidingItems();
          }
        }
      ]
    });
    await alert.present();
  }
}
