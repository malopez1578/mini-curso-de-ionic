import { Component } from "@angular/core";
import { DeseosService } from "src/app/services/deseos.service";
import { Lista } from "src/app/models/lista.model";

@Component({
  selector: "app-tab2",
  templateUrl: "tab2.page.html",
  styleUrls: ["tab2.page.scss"]
})
export class Tab2Page {
  lista: Lista[];
  pendientes;
  constructor(public deseosService: DeseosService) {
    this.getItems();
  }
  getItems() {
    this.lista = this.deseosService.lista;
    this.getTerminados();
  }
  getTerminados() {
    this.pendientes = this.lista.map(elem => {
      elem.items.filter(itemData => !itemData.completado);
    });
  }
}
